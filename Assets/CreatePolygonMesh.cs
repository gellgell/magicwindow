﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif
using OpenCVForUnity;

public class CreatePolygonMesh : MonoBehaviour {
	private Mesh mesh;
	private Vector3 [] initVertices = new Vector3[4];
	private Vector3 [] currentVertices = new Vector3[4];

	public GameObject refObj;
	private realUser mRealUser;

	public GameObject[] spheres = new GameObject[4];

	// Use this for initialization
	void Start () {
		mesh = new Mesh();    
		mRealUser = refObj.GetComponent<realUser> ();
		Vector2 [] newUV       = new Vector2[4];
		int[] newTriangles     = new int[2 * 3];

		// 頂点座標の指定.
		initVertices[0] = new Vector3(-30.0f, 17.5f,  0.0f);
		initVertices[1] = new Vector3(-30.0f, -17.5f, 0.0f);
		initVertices[2] = new Vector3( 30.0f, -17.5f, 0.0f);
		initVertices[3] = new Vector3( 30.0f, 17.5f,  0.0f);

		Vector3 pos = transform.position;

		// UVの指定 (頂点数と同じ数を指定すること).
		newUV[0] = new Vector2(0.0f, 0.0f);
		newUV[1] = new Vector2(0.0f, 1.0f);
		newUV[2] = new Vector2(1.0f, 1.0f);
		newUV[3] = new Vector2(1.0f, 0.0f);

		// 三角形ごとの頂点インデックスを指定.
		newTriangles[0] = 2;
		newTriangles[1] = 1;
		newTriangles[2] = 0;
		newTriangles[3] = 0;
		newTriangles[4] = 3;
		newTriangles[5] = 2;

		mesh.vertices  = initVertices;
		mesh.uv        = newUV;
		mesh.triangles = newTriangles;

		mesh.RecalculateNormals();
		mesh.RecalculateBounds();

		GetComponent<MeshFilter>().sharedMesh = mesh;
		GetComponent<MeshFilter>().sharedMesh.name = "myMesh";
	}

	// Update is called once per frame
	void Update () {

		Mat hm = mRealUser.calcHomography ();
/*
		Debug.Log ("m.ToString" + hm.ToString());
		Debug.Log ("m.dump" + hm.dump());
		Debug.Log ("m.get(0,0)" + hm.get(0,0)[0]);
		Debug.Log ("m.get(0,1)" + hm.get(0,1)[0]);
		Debug.Log ("m.get(0,2)" + hm.get(0,2)[0]);
		Debug.Log ("m.get(1,0)" + hm.get(1,0)[0]);
		Debug.Log ("m.get(1,1)" + hm.get(1,1)[0]);
		Debug.Log ("m.get(1,2)" + hm.get(1,2)[0]);
		Debug.Log ("m.get(2,0)" + hm.get(2,0)[0]);
		Debug.Log ("m.get(2,1)" + hm.get(2,1)[0]);
		Debug.Log ("m.get(2,2)" + hm.get(2,2)[0]);
*/	
		currentVertices [0].x = (float)((initVertices [0].x * hm.get (0, 0) [0] + initVertices [0].y * hm.get (0,1)[0]+hm.get(0,2)[0])/(initVertices[0].x*hm.get(2,0)[0]+initVertices[0].y*hm.get(2,1)[0]+1));
		currentVertices [0].y = (float)((initVertices [0].x * hm.get (1, 0) [0] + initVertices [0].y * hm.get (1,1)[0]+hm.get(1,2)[0])/(initVertices[0].x*hm.get(2,0)[0]+initVertices[0].y*hm.get(2,1)[0]+1));

		currentVertices [1].x = (float)((initVertices [1].x * hm.get (0, 0) [0] + initVertices [1].y * hm.get (0,1)[0]+hm.get(0,2)[0])/(initVertices[1].x*hm.get(2,0)[0]+initVertices[1].y*hm.get(2,1)[0]+1));
		currentVertices [1].y = (float)((initVertices [1].x * hm.get (1, 0) [0] + initVertices [1].y * hm.get (1,1)[0]+hm.get(1,2)[0])/(initVertices[1].x*hm.get(2,0)[0]+initVertices[1].y*hm.get(2,1)[0]+1));

		currentVertices [2].x = (float)((initVertices [2].x * hm.get (0, 0) [0] + initVertices [2].y * hm.get (0,1)[0]+hm.get(0,2)[0])/(initVertices[2].x*hm.get(2,0)[0]+initVertices[2].y*hm.get(2,1)[0]+1));
		currentVertices [2].y = (float)((initVertices [2].x * hm.get (1, 0) [0] + initVertices [2].y * hm.get (1,1)[0]+hm.get(1,2)[0])/(initVertices[2].x*hm.get(2,0)[0]+initVertices[2].y*hm.get(2,1)[0]+1));

		currentVertices [3].x = (float)((initVertices [3].x * hm.get (0, 0) [0] + initVertices [3].y * hm.get (0,1)[0]+hm.get(0,2)[0])/(initVertices[3].x*hm.get(2,0)[0]+initVertices[3].y*hm.get(2,1)[0]+1));
		currentVertices [3].y = (float)((initVertices [3].x * hm.get (1, 0) [0] + initVertices [3].y * hm.get (1,1)[0]+hm.get(1,2)[0])/(initVertices[3].x*hm.get(2,0)[0]+initVertices[3].y*hm.get(2,1)[0]+1));
/*
		for(int i=0; i<4; i++) {
			Debug.Log (i+":"+currentVertices[i].z);
		}
*/
		mesh.vertices  = currentVertices;
	
	}
}


