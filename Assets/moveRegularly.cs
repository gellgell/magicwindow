﻿using UnityEngine;
using System.Collections;

public class moveRegularly : MonoBehaviour {
	public GameObject viewCenter;

	public int amplitude;
	private Vector3 mPos;
	private Vector3 mOffset;

	// Use this for initialization
	void Start () {
		//現状の位置を保持
		//現状の位置を保持
		mOffset = this.transform.position - viewCenter.transform.position;

	}
	
	// Update is called once per frame
	void Update () {
		mPos.x = amplitude * Mathf.Sin (Mathf.PI * 2.0f * (float)Time.frameCount / 180.0f);
		this.transform.localPosition = mPos+mOffset;
		this.transform.LookAt (viewCenter.transform);
	}
}
