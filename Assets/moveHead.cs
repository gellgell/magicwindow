﻿using UnityEngine;
using System.Collections;

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif
using OpenCVForUnity;

public class moveHead : MonoBehaviour {
	public GameObject viewCenter;
	public GameObject refObj;

	public int amplitude;
	private Vector3 mPos;
	public Vector3 mOffset;
	public Camera mainCamera;

	private static Texture2D outputImage;

	private realUser mRealUser;

	private Texture2D screenCap;
	private bool screenFlag = false;

	// Use this for initialization
	void Start () {
		//現状の位置を保持
//		mPos = transform.localPosition;
//		this.transform.localPosition = mPos;
		mainCamera = GetComponent<Camera>();

		mRealUser = refObj.GetComponent<realUser> ();
		screenCap = new Texture2D (mainCamera.pixelWidth, mainCamera.pixelHeight, TextureFormat.RGB24, false);

	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log ("moveHead()");
/*
		mPos.x = amplitude * Mathf.Sin (Mathf.PI * 2.0f * (float)Time.frameCount / 180.0f);
		this.transform.localPosition = mPos+mOffset;
		this.transform.LookAt (viewCenter.transform);
*/
		StartCoroutine ("screenCapture");

/*
		//inputImage must be Texture2D type
		Texture2D inputImage = convertRenderTexToTex2D (rendTex);
		//		Texture2D inputImage = new Texture2D (mainCamera.targetTexture.width, mainCamera.targetTexture.height);

		inputImage.Apply ();
		outputPlane.GetComponent<Renderer> ().material.mainTexture = inputImage;

		//dstImage (Mat type)
		Mat dstImage = new Mat (300, 300, CvType.CV_32FC4);

		//convert texture to OpenCV 
		Mat img = new Mat (rendTex.height, rendTex.width, CvType.CV_32FC4);
		Utils.texture2DToMat (inputImage, img);

		Mat M = new Mat (3, 3, CvType.CV_32FC1);
		M = mRealUser.calcHomography();
		Imgproc.warpPerspective (img, dstImage, M, new Size(300, 300));

//generate output Image
//convert openCV Mat to texture
		outputImage = new Texture2D (300, 300, TextureFormat.RGB24, false);
		Utils.matToTexture2D (dstImage, outputImage);
//		outputImage.Apply ();
//		outputPlane.GetComponent<Renderer> ().material.mainTexture = outputImage;
*/
/*
		mainCamera = GetComponent<Camera> ();
		RenderTexture target = mainCamera.targetTexture;
		if(target == null) {
			Debug.Log ("tartget is null");
		}
		RenderResult = new Texture2D( target.width, target.height, TextureFormat.ARGB32, true );
		UnityEngine.Rect rect = new UnityEngine.Rect( 0, 0, target.width, target.height );
		RenderResult.ReadPixels(rect, 0, 0, true );
*/

		if (screenFlag) {
//			screenCap = convertRenderTexToTex2D (mainCamera.targetTexture);
//			screenCap = RTImage(mainCamera);

//			mRenderTexture = mainCamera.targetTexture;
/*
			RenderTexture.active = mainCamera.targetTexture;
			screenCap.ReadPixels (new UnityEngine.Rect (0, 0, mainCamera.pixelWidth, mainCamera.pixelHeight), 0, 0);
			screenCap.Apply ();
			outputImage = screenCap;
			outputImage.Apply ();
*/

			Debug.Log ("update outputImage");

			Mat src = new Mat (screenCap.height, screenCap.width, CvType.CV_8UC3);
			Utils.fastTexture2DToMat (screenCap, src);

			Mat H = mRealUser.calcHomography ();

			Mat dst = new Mat (screenCap.height, screenCap.width, CvType.CV_8UC3);

			Imgproc.warpPerspective (src, dst, H, new OpenCVForUnity.Size (src.cols(), src.rows()));

			//幅：640 高さ：400
//			Debug.Log ("col"+src.cols()+"rows"+src.rows());

			outputImage = new Texture2D (screenCap.width, screenCap.height, TextureFormat.RGB24, false);
			Utils.fastMatToTexture2D (dst, outputImage);
			outputImage.Apply ();

			screenFlag = false;
		}
	}

	public static Texture2D convertRenderTexToTex2D (RenderTexture rt) {

		RenderTexture.active = rt;
		Texture2D texture = new Texture2D (rt.width, rt.height, TextureFormat.RGB24, false);
		texture.ReadPixels (new UnityEngine.Rect (0, 0, texture.width, texture.height), 0, 0);

		return texture;

	}

	public Texture2D getOutputImage() {
		return outputImage;
	}

	IEnumerator screenCapture() {
		Debug.Log ("screenCapture()");

		yield return new WaitForEndOfFrame ();
		RenderTexture.active = mainCamera.targetTexture;
		screenCap = new Texture2D (mainCamera.pixelWidth, mainCamera.pixelHeight, TextureFormat.RGB24, false);
		screenCap.ReadPixels (new UnityEngine.Rect (0, 0, mainCamera.pixelWidth, mainCamera.pixelHeight), 0, 0);
//		screenCap.Apply ();
//		outputImage = screenCap;
//		outputImage.Apply ();

		RenderTexture.active = null;

		screenFlag = true;
	}
/*
	public Texture2D RTImage(Camera cam) {
		RenderTexture currentRT = RenderTexture.active;
		RenderTexture.active = cam.targetTexture;
		cam.Render();
		Texture2D image = new Texture2D(cam.targetTexture.width, cam.targetTexture.height, TextureFormat.RGB24, false);
		image.ReadPixels(new UnityEngine.Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), 0, 0);

		Debug.Log ("image");
		Debug.Log ("w:"+image.width+"h:"+image.height);
		image.Apply();
		RenderTexture.active = currentRT;
		return image;
	}
*/
}


