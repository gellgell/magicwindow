﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class readTexture : MonoBehaviour {

	public GameObject refObj;
	moveHead mMoveHead;
//	realUser mRealUser;

	public Renderer display;

	// Use this for initialization
	void Start () {
		mMoveHead = refObj.GetComponent<moveHead> ();
//		mRealUser = refObj.GetComponent<realUser>();
	}
	
	// Update is called once per frame
	void Update () {
		updateTexture ();
		if(Input.GetKeyUp(KeyCode.D)) {
			updateTexture();
		}
	}

	public void updateTexture() {
		Debug.Log ("readTexture");
		Texture2D tex = mMoveHead.getOutputImage ();
//		Texture2D tex = mRealUser.getOutputImage();
		if(tex != null) {
			Debug.Log ("update mainTexture");
//			tex.ReadPixels(new UnityEngine.Rect(0,0,tex.width, tex.height),0,0);
			//			Debug.Log ("width:"+tex.width+"height"+tex.height);
//			tex.Apply ();
			float check_time = Time.realtimeSinceStartup;
			display.material.mainTexture = tex;
			check_time = Time.realtimeSinceStartup - check_time;
			Debug.Log( "check time : " + check_time.ToString("0.00000") );

		}
	}
}
