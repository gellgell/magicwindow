﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif
using OpenCVForUnity;


public class realUser : MonoBehaviour {
	public GameObject viewCenter;
	public GameObject display;
	public GameObject[] vertex = new GameObject[4];

	public int amplitude;
	private Vector3 mPos;
//	public Vector3 mOffset;

//	moveHead mHead = virtualUser.GetComponent<moveHead>();
//	private Texture2D outputImage = 

	private static Camera mainCamera;

	private const int cPoints = 4; 

	private Texture2D screenCap;
	private bool screenFlag = false;

	private static Texture2D outputImage;

	// Use this for initialization
	void Start () {
		//現状の位置を保持
//		mOffset = this.transform.position - display.transform.localPosition;

		mainCamera = GetComponent<Camera>();
		screenCap = new Texture2D (mainCamera.pixelWidth, mainCamera.pixelHeight, TextureFormat.RGB24, false);
	}
	
	// Update is called once per frame
	void Update () {
/*
		mPos.x = amplitude * Mathf.Sin (Mathf.PI * 2.0f * (float)Time.frameCount / 180.0f);
		this.transform.position = mPos+mOffset;
		this.transform.LookAt (viewCenter.transform);
*/
//		StartCoroutine ("screenCapture");
	}

	public Mat calcHomography() {
		Debug.Log ("calcHomography");
		Mat src;
		Mat dst;

		Vector3[] vertexPoint = new Vector3[4];

		// 極めて重要
		for (int i = 0; i < cPoints; i++) {
			vertexPoint [i] = mainCamera.WorldToScreenPoint (vertex [i].transform.position);
			vertexPoint[i].y = mainCamera.pixelHeight - vertexPoint [i].y;
		}
/*
		for (int i = 0; i < cPoints; i++) {
			Debug.Log (i + "x:" + vertexPoint [i].x + " y:" + vertexPoint [i].y);
		}
*/
		Vector3[] imageVertexPoint = new Vector3[4];
/*
		imageVertexPoint [0].x = 0;
		imageVertexPoint [0].y = Screen.height;
		imageVertexPoint [1].x = 0;
		imageVertexPoint [1].y = 0;
		imageVertexPoint [2].x = Screen.width;
		imageVertexPoint [2].y = 0;
		imageVertexPoint [3].x = Screen.width;
		imageVertexPoint [3].y = Screen.height;
*/

		imageVertexPoint [0].x = 0;
		imageVertexPoint [0].y = 0;
		imageVertexPoint [1].x = 0;
		imageVertexPoint [1].y = mainCamera.pixelHeight;
		imageVertexPoint [2].x = mainCamera.pixelWidth;
		imageVertexPoint [2].y = mainCamera.pixelHeight;
		imageVertexPoint [3].x = mainCamera.pixelWidth;
		imageVertexPoint [3].y = 0;
/*
		Debug.Log ("vertexPoints");
		for(int i=0; i<4; i++) {
			Debug.Log (vertexPoint[i].x+","+vertexPoint[i].y);
		}

		Debug.Log ("imageVertexPoints");
		for(int i=0; i<4; i++) {
			Debug.Log (imageVertexPoint[i].x+","+imageVertexPoint[i].y);
		}			
*/
		//input features
		src = new Mat (4,1, CvType.CV_32FC2);
		// topleft.x topleft.y botleft.x botleft.y botright.x botright.y topright.x topright.y
		src.put (0,0, vertexPoint[0].x, vertexPoint[0].y, vertexPoint[1].x, vertexPoint[1].y, 
			vertexPoint[2].x, vertexPoint[2].y, vertexPoint[3].x, vertexPoint[3].y);
/*
		Debug.Log ("src");
		Debug.Log ("src.dump" + src.dump());
*/
		//dst features
		dst = new Mat (4, 1, CvType.CV_32FC2);
		dst.put (0,0, imageVertexPoint[0].x, imageVertexPoint[0].y, imageVertexPoint[1].x, imageVertexPoint[1].y, 
			imageVertexPoint[2].x, imageVertexPoint[2].y, imageVertexPoint[3].x, imageVertexPoint[3].y);
/*
		Debug.Log ("dst");
		Debug.Log ("dst.dump" + dst.dump());
*/
		//Homography
//		Mat H = Imgproc.getPerspectiveTransform (src, dst);

		List<Point> obj = new List<Point> ();
		List<Point> scene = new List<Point> ();

		for(int i=0; i<4; i++) {
			obj.Add (new Point(vertexPoint[i].x, vertexPoint[i].y));
			scene.Add (new Point(imageVertexPoint[i].x, imageVertexPoint[i].y));
		}
			
		MatOfPoint2f matObj = new MatOfPoint2f (obj.ToArray());
		MatOfPoint2f matScene = new MatOfPoint2f (scene.ToArray());

		Mat H = Calib3d.findHomography(matObj, matScene);

/*
		Debug.Log("Homography");
		Debug.Log ("H.ToString"+ H.ToString());
		Debug.Log ("H.dump" + H.dump());

		Vector3[] chackPoint = new Vector3[4];
		chackPoint [0].x = (float)((vertexPoint [0].x * H.get (0, 0) [0] + vertexPoint [0].y * H.get (0,1)[0]+H.get(0,2)[0])/(vertexPoint[0].x*H.get(2,0)[0]+vertexPoint[0].y*H.get(2,1)[0]+1));
		chackPoint [0].y = (float)((vertexPoint [0].x * H.get (1, 0) [0] + vertexPoint [0].y * H.get (1,1)[0]+H.get(1,2)[0])/(vertexPoint[0].x*H.get(2,0)[0]+vertexPoint[0].y*H.get(2,1)[0]+1));

		chackPoint [1].x = (float)((vertexPoint [1].x * H.get (0, 0) [0] + vertexPoint [1].y * H.get (0,1)[0]+H.get(0,2)[0])/(vertexPoint[1].x*H.get(2,0)[0]+vertexPoint[1].y*H.get(2,1)[0]+1));
		chackPoint [1].y = (float)((vertexPoint [1].x * H.get (1, 0) [0] + vertexPoint [1].y * H.get (1,1)[0]+H.get(1,2)[0])/(vertexPoint[1].x*H.get(2,0)[0]+vertexPoint[1].y*H.get(2,1)[0]+1));

		chackPoint [2].x = (float)((vertexPoint [2].x * H.get (0, 0) [0] + vertexPoint [2].y * H.get (0,1)[0]+H.get(0,2)[0])/(vertexPoint[2].x*H.get(2,0)[0]+vertexPoint[2].y*H.get(2,1)[0]+1));
		chackPoint [2].y = (float)((vertexPoint [2].x * H.get (1, 0) [0] + vertexPoint [2].y * H.get (1,1)[0]+H.get(1,2)[0])/(vertexPoint[2].x*H.get(2,0)[0]+vertexPoint[2].y*H.get(2,1)[0]+1));

		chackPoint [3].x = (float)((vertexPoint [3].x * H.get (0, 0) [0] + vertexPoint [3].y * H.get (0,1)[0]+H.get(0,2)[0])/(vertexPoint[3].x*H.get(2,0)[0]+vertexPoint[3].y*H.get(2,1)[0]+1));
		chackPoint [3].y = (float)((vertexPoint [3].x * H.get (1, 0) [0] + vertexPoint [3].y * H.get (1,1)[0]+H.get(1,2)[0])/(vertexPoint[3].x*H.get(2,0)[0]+vertexPoint[3].y*H.get(2,1)[0]+1));

		Debug.Log ("chackPoints");
		for(int i=0; i<4; i++) {
			Debug.Log ((int)chackPoint[i].x+","+(int)chackPoint[i].y);
		}			
*/

/*			
		Debug.Log ("update outputImage");
		Mat srcMat = new Mat (screenCap.height, screenCap.width, CvType.CV_8UC3);
		Utils.fastTexture2DToMat (screenCap, srcMat);

		Mat dstMat = new Mat (screenCap.height, screenCap.width, CvType.CV_8UC3);
		Imgproc.warpPerspective (srcMat, dstMat, H, new OpenCVForUnity.Size (srcMat.cols(), srcMat.rows()), Imgproc.INTER_LINEAR);

		outputImage = new Texture2D (screenCap.width, screenCap.height, TextureFormat.RGB24, false);
		Utils.fastMatToTexture2D (dstMat, outputImage);
//		outputImage.Apply ();

		screenFlag = false;
*/
		return H;
	}

	IEnumerator screenCapture() {
		Debug.Log ("screenCapture()");

		yield return new WaitForEndOfFrame ();

		RenderTexture.active = mainCamera.targetTexture;
		screenCap = new Texture2D (mainCamera.pixelWidth, mainCamera.pixelHeight, TextureFormat.RGB24, false);
		screenCap.ReadPixels (new UnityEngine.Rect (0, 0, mainCamera.pixelWidth, mainCamera.pixelHeight), 0, 0);
//		screenCap.Apply ();

//		outputImage = screenCap;
//		outputImage.Apply ();

		RenderTexture.active = null;

		screenFlag = true;
	}

	public Texture2D getOutputImage() {
		if (screenFlag)
			calcHomography (); 

		return outputImage;
	}
}
